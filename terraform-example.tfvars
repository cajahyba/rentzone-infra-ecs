## VPC variables

region       = "sa-east-1"
project_name = "cajahyba-test"
environment  = "dev"

vpc_cidr                     = "10.0.0.0/16"
public_subnet_az1_cidr       = "10.0.0.0/24"
public_subnet_az2_cidr       = "10.0.1.0/24"
private_app_subnet_az1_cidr  = "10.0.2.0/24"
private_app_subnet_az2_cidr  = "10.0.3.0/24"
private_data_subnet_az1_cidr = "10.0.4.0/24"
private_data_subnet_az2_cidr = "10.0.5.0/24"

ssh_ip = "your_machine_ip"

database_snapshot_identifier = ""
database_instance_class      = "db.t2.micro"
database_instance_identifier = "dev-rds-db"
multi_az_deployment          = "false"

db_name              = "applicationdb"
db_username          = "db_username"
db_password          = "db_password"
db_allocated_storage = 200

domain_name       = "your_domain.com" # this is the hosted zone
alternative_names = "*.your_domain.com"

target_type = "ip"

env_file_bucket_name = "your_env_file-ecs-env-file-bucket"
env_file_name        = "your_env_file"


architecture    = "X86_64"
container_image = "your_container_image_in_ecr"

record_name = "www"