# rentzone-backend-ecs

This project goal is to use [Terraform-Modules](https://gitlab.com/cajahyba/aws-terraform-modules) project to create/maintain AWS infrastructure.

The following architecture should be generated:

![AWS Infrastructure](AWS_Architectures.png "AWS Infrastructure").

## AWS Sevices/Features used in this project

## S3

Amazon Simple Storage Service (Amazon S3) is an object storage service that offers industry-leading scalability, data availability, security, and performance. 

This service is used to store the Terraform state file and the NodeJS env file.

## DynamoDB

Amazon DynamoDB is a fully managed NoSQL database service that provides fast and predictable performance with seamless scalability. 

This service is used to store the Terraform lock file info. This will avoid two people make changes in the infrastructure at the same time.

## IAM

## RDS

## Route 53

## ACM - Certificate Manager

## ALB - Application Load Balancer

## ASG - Auto-scaling group

## ECR - Container Registry

## ECS + Fargate - Container Services

## Nat - Gateway

## VPC