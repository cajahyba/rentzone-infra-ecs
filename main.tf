locals {
  region       = var.region
  project_name = var.project_name
  environment  = var.environment
}

# Create VPC module
module "vpc" {
  source = "git@gitlab.com:cajahyba/aws-terraform-modules.git//vpc"

  region       = local.region
  project_name = local.project_name
  environment  = local.environment

  vpc_cidr                     = var.vpc_cidr
  public_subnet_az1_cidr       = var.public_subnet_az1_cidr
  public_subnet_az2_cidr       = var.public_subnet_az2_cidr
  private_app_subnet_az1_cidr  = var.private_app_subnet_az1_cidr
  private_app_subnet_az2_cidr  = var.private_app_subnet_az2_cidr
  private_data_subnet_az1_cidr = var.private_data_subnet_az1_cidr
  private_data_subnet_az2_cidr = var.private_data_subnet_az2_cidr
}

# Create nat-gateways
module "nat_gateway" {
  source = "git@gitlab.com:cajahyba/aws-terraform-modules.git//nat-gateway"

  project_name = local.project_name
  environment  = local.environment

  internet_gateway           = module.vpc.internet_gateway
  vpc_id                     = module.vpc.vpc_id
  public_subnet_az1_id       = module.vpc.public_subnet_az1_id
  public_subnet_az2_id       = module.vpc.public_subnet_az2_id
  private_app_subnet_az1_id  = module.vpc.private_app_subnet_az1_id
  private_app_subnet_az2_id  = module.vpc.private_app_subnet_az2_id
  private_data_subnet_az1_id = module.vpc.private_data_subnet_az1_id
  private_data_subnet_az2_id = module.vpc.private_data_subnet_az2_id
}

# Create security groups
module "security_group" {
  source = "git@gitlab.com:cajahyba/aws-terraform-modules.git//security-groups"

  # security groups variables
  project_name = local.project_name
  environment  = local.environment

  vpc_id = module.vpc.vpc_id
  ssh_ip = var.ssh_ip

}

# Launch RDS instance
module "rds" {
  source = "git@gitlab.com:cajahyba/aws-terraform-modules.git//rds"

  # RDS variables
  project_name = local.project_name
  environment  = local.environment

  private_data_subnet_az1_id   = module.vpc.private_data_subnet_az1_id
  private_data_subnet_az2_id   = module.vpc.private_data_subnet_az2_id
  database_instance_class      = var.database_instance_class
  database_snapshot_identifier = var.database_snapshot_identifier
  database_instance_identifier = var.database_instance_identifier
  multi_az_deployment          = var.multi_az_deployment
  availability_zone_1          = module.vpc.availability_zone_1
  database_security_group_id   = module.security_group.database_security_group_id

  db_name              = var.db_name
  db_username          = var.db_username
  db_password          = var.db_password
  db_allocated_storage = var.db_allocated_storage

}
# request ssl certificate
module "ssl_certificate" {
  source = "git@gitlab.com:cajahyba/aws-terraform-modules.git//acm"

  # ACM variables
  domain_name       = var.domain_name
  alternative_names = var.alternative_names

}

# Create Application Load Balancer
module "application_load_balancer" {
  source = "git@gitlab.com:cajahyba/aws-terraform-modules.git//alb"

  # ALB variables
  project_name = local.project_name
  environment  = local.environment

  alb_security_group_id = module.security_group.alb_security_group_id
  public_subnet_az1_id  = module.vpc.public_subnet_az1_id
  public_subnet_az2_id  = module.vpc.public_subnet_az2_id
  target_type           = var.target_type
  vpc_id                = module.vpc.vpc_id
  certificate_arn       = module.ssl_certificate.certificate_arn

}

# Create S3 Bucket
module "s3_bucket" {
  source = "git@gitlab.com:cajahyba/aws-terraform-modules.git//s3"

  # ALB variables
  project_name         = local.project_name
  env_file_bucket_name = var.env_file_bucket_name
  env_file_name        = var.env_file_name
}

# Create ECR Repositories
module "ecr" {
  source = "git@gitlab.com:cajahyba/aws-terraform-modules.git//ecr"

  # ECR variables
  project_name = local.project_name
  environment  = local.environment
}

# Create ECS Task execution role (IAM)
module "ecs_task_execution_role" {
  source = "git@gitlab.com:cajahyba/aws-terraform-modules.git//iam"


  project_name         = local.project_name
  environment          = local.environment
  env_file_bucket_name = module.s3_bucket.env_file_bucket_name

}

# Create ECS cluster, task definition and service
module "ecs" {
  source       = "git@gitlab.com:cajahyba/aws-terraform-modules.git//ecs"
  region       = local.region
  project_name = local.project_name
  environment  = local.environment

  ecs_task_execution_role_arn  = module.ecs_task_execution_role.ecs_task_execution_role_arn
  architecture                 = var.architecture
  container_image              = var.container_image
  env_file_name                = module.s3_bucket.env_file_name
  env_file_bucket_name         = module.s3_bucket.env_file_bucket_name
  private_app_subnet_az1_id    = module.vpc.private_app_subnet_az1_id
  private_app_subnet_az2_id    = module.vpc.private_app_subnet_az2_id
  app_server_security_group_id = module.security_group.app_server_security_group_id
  alb_target_group_arn         = module.application_load_balancer.alb_target_group_arn
}

# Create Auto Scaling Group
module "ecs_asg" {
  source = "git@gitlab.com:cajahyba/aws-terraform-modules.git//asg-ecs"

  project_name = local.project_name
  environment  = local.environment
  ecs_service  = module.ecs.ecs_service

} 

#Create Auto Scaling Group
module "route_53" {
  source = "git@gitlab.com:cajahyba/aws-terraform-modules.git//route53"

  domain_name                        = module.ssl_certificate.domain_name
  record_name                        = var.record_name
  application_load_balancer_dns_name = module.application_load_balancer.application_load_balancer_dns_name
  application_load_balancer_zone_id  = module.application_load_balancer.application_load_balancer_zone_id

}

# Print the website URL
output "website_url" {
  value = join("", ["https://", var.record_name, ".", var.domain_name])
}