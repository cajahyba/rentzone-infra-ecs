# store the terraform state file in s3 and lock with dynamodb
# we need to create this bucket manually before we use it.
# get its info on AWS Console and use them here.
terraform {
  backend "s3" {
    bucket         = "cajahyba-terraform-remote-state"
    key            = "terraform-module/state/terraform.tfstate"
    region         = "sa-east-1"
    profile        = "terraform-user"
    dynamodb_table = "terraform-state-lock"
  }
}